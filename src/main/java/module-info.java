module com.waffle.jake.please.respond {
    requires java.base;
    requires java.net.http;
    
    requires com.fasterxml.jackson.databind;
    
    // slf4j's module descriptor declares that it needs a SLF4JServiceProvider 
    // and Logback will be used to supply the service provider implementation.
    // https://stackoverflow.com/questions/54777923/logback-in-a-java-9-modular-application-not-working
    requires org.slf4j;

    // This is technically a runtime dependency that supports SLF4j's service provider,
    // but we either need to require the module or the logback-classic.jar and 
    // logback-core.jar must be included in the jlink --module-path.
    // Requiring the module just makes building/running a bit simpler, so that's what we're doing.
    requires ch.qos.logback.classic;
    
    requires com.google.common;

    exports com.waffle.jake.please.respond;
    opens com.waffle.jake.please.respond.rsvp.dto;
}