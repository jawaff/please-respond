/**
 * Copyright 2020 Jake Waffle
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.waffle.jake.please.respond.rsvp.dto.adapters;

import java.io.IOException;
import java.time.Instant;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * The InstantDeserializer class is responsible for adding {@code Instant} 
 * serialization support to {@code ObjectMapper}.
 */
final class InstantSerializer extends StdSerializer<Instant>
{

	private static final long serialVersionUID = -7980254099369415693L;

    InstantSerializer()
    {
        super(Instant.class);
    }

    @Override
    public void serialize(final Instant value, final JsonGenerator generator, final SerializerProvider provider)
      throws IOException
    {
        generator.writeNumber(value.toEpochMilli());
    }
}