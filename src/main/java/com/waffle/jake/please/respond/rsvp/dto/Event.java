/**
 * Copyright 2020 Jake Waffle
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.waffle.jake.please.respond.rsvp.dto;

import java.time.Instant;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class Event {
    private final Instant time;
    private final String url;
    
	@JsonCreator
	private Event(@JsonProperty("time") final Instant time, @JsonProperty("event_url") final String url) {
	    this.time = Objects.requireNonNull(time, "Event time must not be null.");
	    this.url = Objects.requireNonNull(url, "Event url must not be null.");
	}

	public static Event create(final Instant time, final String url) {
		return new Event(time, url);
	}
    
	@JsonGetter("time")
    public Instant getTime() {
    	return this.time;
    }
    
	@JsonGetter("event_url")
    public String getUrl() {
    	return this.url;
    }
}
