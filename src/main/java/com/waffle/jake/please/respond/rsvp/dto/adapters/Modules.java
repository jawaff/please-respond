/**
 * Copyright 2020 Jake Waffle
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.waffle.jake.please.respond.rsvp.dto.adapters;

import java.time.Instant;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * The Modules class is responsible for creating JSON modules.
 */
public final class Modules
{
  /**
   * Creates and initializes the rsvp module.
   * @return The rsvp module.
   */
  public static SimpleModule rsvp()
  {
    final SimpleModule module = new SimpleModule("RSVP", new Version(1, 0, 0, null, null, null));

    //
    // Instant
    //
    module.addSerializer(Instant.class, new InstantSerializer());
    module.addDeserializer(Instant.class, new InstantDeserializer());
    
    return module;
  }

  /**
   * Private constructor indicates that an instance of this class should never be
   * constructed.
   */
  private Modules()
  {
    throw new AssertionError(String.format("Illegal instantiation of class: %s.", this.getClass().getName()));
  }
}