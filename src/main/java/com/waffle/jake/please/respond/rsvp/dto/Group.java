/**
 * Copyright 2020 Jake Waffle
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.waffle.jake.please.respond.rsvp.dto;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Preconditions;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class Group {
    private final String country;
    
	@JsonCreator
	private Group(@JsonProperty("group_country") final String country) {
	    Objects.requireNonNull(country, "Group country must not be null.");
		Preconditions.checkArgument(
				country.length() == 2,
				"Country string must have a length of 2: %s",
				country);
		this.country = country;
	}

	public static Group create(final String country) {
		return new Group(country);
	}

	@JsonGetter("group_country")
    public String getCountry() {
    	return this.country;
    }
}
