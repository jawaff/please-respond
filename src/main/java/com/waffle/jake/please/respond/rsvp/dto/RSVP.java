/**
 * Copyright 2020 Jake Waffle
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.waffle.jake.please.respond.rsvp.dto;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class RSVP {
	private final Event event;
	private final Group group;

	@JsonCreator
	private RSVP(@JsonProperty("event") final Event event, @JsonProperty("group") final Group group) {
	    this.event = Objects.requireNonNull(event, "RSVP Event must not be null.");
	    this.group = Objects.requireNonNull(group, "RSVP Group must not be null.");
	}
	
	public static RSVP create(final Event event, final Group group) {
		return new RSVP(event, group);
	}

	@JsonGetter("event")
	public Event getEvent() {
		return this.event;
	}

	@JsonGetter("group")
	public Group getGroup() {
		return this.group;
	}
}
