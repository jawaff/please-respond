/**
 * Copyright 2020 Jake Waffle
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.waffle.jake.please.respond.rsvp.dto;

import java.util.Objects;

import com.google.common.base.Preconditions;

/**
 * Stores the RSVP information for a particular country.
 */
public final class RSVPCountryInfo {
	private final String country;
	private final long rsvpCount;
	
	/**
	 * @param country The two character country name.
	 * @throws NullPointerException if country is null.
	 * @throws IllegalArgumentException if country string doesn't have a length of 2. 
	 * @param count The number of RSVPs witnessed for the associated country.
	 * @throws IllegalArgumentException if RSVP count is 0 or less.
	 */
	public RSVPCountryInfo(final String country, final long count) {
		Objects.requireNonNull(country, "Event country must not be null.");
		Preconditions.checkArgument(
				country.length() == 2,
				"Country string must have a length of 2: %s",
				country);
		this.country = country;
		
		Preconditions.checkArgument(
				count > 0, 
				"Country RSVP count must be greater than zero: %s", 
				count);
		this.rsvpCount = count;
	}
	
	public String getCountry() {
		return this.country;
	}

	public long getCount() {
		return this.rsvpCount;
	}
	
	/**
	 * @return A comma-delimited representation of the RSVP country info.
	 */
	@Override
	public String toString() {
		return String.format(
				"%s,%d",
				this.country,
				this.rsvpCount);
	}
}
