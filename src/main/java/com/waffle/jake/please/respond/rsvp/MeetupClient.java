/**
 * Copyright 2020 Jake Waffle
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.waffle.jake.please.respond.rsvp;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class MeetupClient implements IMeetupClient {
	private static final Logger LOGGER = LoggerFactory.getLogger(MeetupClient.class);
	private static final String RSVP_STREAM_URL = "https://stream.meetup.com:443/2/rsvps";
	
	/**
	 *  Immutable Http Client (thread-safe)
	 */
	private static final HttpClient HTTP_CLIENT = HttpClient.newHttpClient();
    
	@Override
	public InputStream getRSVPStream() {
		final InputStream rsvpStream;
    	try {
    		final HttpRequest request = HttpRequest.newBuilder(new URI(RSVP_STREAM_URL))
					.header("Content-Type", "application/json")
					.GET()
					.build();
    		
			final HttpResponse<InputStream> response = HTTP_CLIENT.send(
					request,
					BodyHandlers.ofInputStream());
			
			if (response.statusCode() != 200) {
				throw new IllegalStateException(String.format(
						"RSVP data stream request has failed with status code: %d, %s",
						response.statusCode(),
						response.headers().map().get("Location").get(0)));
			}
			
			rsvpStream = response.body();
		} catch (URISyntaxException e) {
			final String msg = "Failed creating HttpRequest for RSVP stream.";
			LOGGER.error(msg);
			throw new IllegalStateException(msg, e);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			final String msg = "RSVP stream has been interrupted.";
			LOGGER.error(msg);
			throw new IllegalStateException(msg, e);
		} catch (IOException e) {
			final String msg = "Failed getting RSVP stream.";
			LOGGER.error(msg);
            throw new IllegalStateException(msg, e);
		}
    	return rsvpStream;
	}
}
