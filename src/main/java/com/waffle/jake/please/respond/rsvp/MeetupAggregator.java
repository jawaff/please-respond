/**
 * Copyright 2020 Jake Waffle
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.waffle.jake.please.respond.rsvp;

import java.io.IOException;
import java.io.InputStream;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.waffle.jake.please.respond.rsvp.dto.RSVP;
import com.waffle.jake.please.respond.rsvp.dto.RSVPCountryInfo;
import com.waffle.jake.please.respond.rsvp.dto.adapters.Modules;
import com.waffle.jake.please.respond.rsvp.util.RSVPComparator;

public final class MeetupAggregator {
	private static final Logger LOGGER = LoggerFactory.getLogger(MeetupAggregator.class);

	/**
	 * The maximum number of country infos expected.
	 */
	private static final int MAX_COUNTRY_INFO_COUNT = 3;

	/**
	 *  Immutable Mapper (thread-safe)
	 */
	private static final ObjectMapper MAPPER = new ObjectMapper()
			.registerModule(Modules.rsvp());
	
	private final IMeetupClient meetupClient;
	
	/**
	 * @param meetupClient The client that is to be used for interacting
	 * with Meetup.com.
	 * @throws NullPointerException if meetupClient is null.
	 */
    public MeetupAggregator(final IMeetupClient meetupClient) {
    	this.meetupClient = Objects.requireNonNull(meetupClient, "Meetup client must not be null.");
    }
    
	/**
	 * Blocks for the specified time period and collects RSVP information from
	 * a data stream.
	 * @param collectionSeconds The amount of seconds that we will block and
	 * wait for RSVPs.
	 * @throws IllegalArgumentException if collectionSeconds is 0 or less.
	 * @return The list of RSVPs that were collected.
	 */
    private List<RSVP> getRSVPs(final int collectionSeconds) {
    	final InputStream rsvpStream = this.meetupClient.getRSVPStream();
    	
    	final List<RSVP> rsvps = new ArrayList<>();
    	try {
    	    final JsonParser parser = MAPPER.getFactory().createParser(rsvpStream);
    	    final long startMillis = System.currentTimeMillis();
    	    long elapsedSeconds = 0;
    	    
    	    // While we're still waiting and there's a token available.
    	    while (elapsedSeconds < collectionSeconds && parser.nextToken() != null) {
    	    	rsvps.add(parser.readValueAs(RSVP.class));
    	    	elapsedSeconds = (System.currentTimeMillis() - startMillis) / 1_000;
    	    }
    	} catch (IOException e) {
    		final String msg = "Failed to parse RSVP event stream.";
    		LOGGER.error(msg);
    		throw new IllegalStateException(msg, e);
    	}
    	return rsvps;
    }
    
    /**
     * Searches through the provided list for the RSVP with the latest time.
     * @param rsvps The RSVPs of interest.
     * @return The latest RSVP or nothing if the provided list is empty.
     */
    Optional<RSVP> getLatestRSVP(final List<RSVP> rsvps) {
    	final Optional<RSVP> latestRSVP;
    	if (rsvps.isEmpty()) {
    		latestRSVP = Optional.empty();
    	} else {
    		rsvps.sort(new RSVPComparator());
    		latestRSVP = Optional.of(rsvps.get(rsvps.size() - 1));
    	}
    	return latestRSVP;
    }
    
    /**
     * Counts the provided RSVPs based on the country they're being hosted 
     * within and returns a reverse-ordered (highest count first) list of 
     * the RSVP country information.
     * @param rsvps The RSVPs of interest.
     * @return The reverse-ordered list of RSVP country information.
     */
    List<RSVPCountryInfo> getReverseOrderedCountryInfos(final Collection<RSVP> rsvps) {
    	return rsvps.stream()
    			// Returns a mapping of countries to their associated RSVP count.
    	        .collect(Collectors.groupingBy(
    	        		(RSVP rsvp) -> rsvp.getGroup().getCountry(),
    	        		Collectors.counting()))
    	        .entrySet()
    	        .stream()
    	        .map(entry -> new RSVPCountryInfo(entry.getKey(), entry.getValue()))
    	        // Sorts in reverse order by the RSVP country counts (highest count is first).
    	        .sorted(Collections.reverseOrder(
    	        		Comparator.comparingLong(RSVPCountryInfo::getCount)))
    	        .collect(Collectors.toList());
    }
    
    /**
     * Blocks while retrieving RSVP information for the specified time and
     * then returns an aggregated summary of the witnessed RSVPs. 
     * @param collectionSeconds The number of seconds to wait for RSVP events.
     * @return The RSVP aggregation info String.
     */
    public String getRSVPAggegationInfo(final int collectionSeconds) {
    	// Get the RSVPs.
    	final List<RSVP> rsvps = this.getRSVPs(collectionSeconds);
    	
    	// Process the RSVPs to get the information we need.
    	final Optional<RSVP> latestRSVPOpt = this.getLatestRSVP(rsvps);
    	final String latestRSVPDate = latestRSVPOpt.map(rsvp ->
                        DateTimeFormatter.ISO_INSTANT.format(rsvp.getEvent().getTime()))
    			// If no latest RSVP exists, we use "n/a".
                .orElse("n/a");
    	final String latestRSVPUrl = latestRSVPOpt.map(rsvp -> rsvp.getEvent().getUrl())
    			// If no latest RSVP exists, we use "n/a".
    			.orElse("n/a");
    	
    	final List<RSVPCountryInfo> orderedRSVPCountryCount = this.getReverseOrderedCountryInfos(rsvps);
    	
    	// Finally join the information together.
    	final StringJoiner rsvpInfoJoiner = new StringJoiner(",")
    			.add(Integer.toString(rsvps.size()))
    	        .add(latestRSVPDate)
    	        .add(latestRSVPUrl);
    	
    	// Iterates over the expected number of country entries and adds that number of entries to the StringJoiner.
    	for (int i = 0; i < MAX_COUNTRY_INFO_COUNT; i++) {
    		// We may not have enough entries.
    		if (i < orderedRSVPCountryCount.size()) {
    			final RSVPCountryInfo rsvpCountryInfo = orderedRSVPCountryCount.get(i);
    			rsvpInfoJoiner.add(rsvpCountryInfo.toString());
    		} else {
    			// If we're short on entries, then we'll use "n/a".
    			rsvpInfoJoiner.add("n/a,n/a");
    		}
    	}
    	
    	return rsvpInfoJoiner.toString();
    }
}
