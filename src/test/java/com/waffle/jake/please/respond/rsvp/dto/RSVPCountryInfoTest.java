/**
 * Copyright 2020 Jake Waffle
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.waffle.jake.please.respond.rsvp.dto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RSVPCountryInfoTest {

    @Test
    public void creationTest() {
    	Assertions.assertThrows(NullPointerException.class, () -> {new RSVPCountryInfo(null, 1);});
    	Assertions.assertThrows(IllegalArgumentException.class, () -> {new RSVPCountryInfo("a", 2);});
    	Assertions.assertThrows(IllegalArgumentException.class, () -> {new RSVPCountryInfo("asd", 3);});
    	Assertions.assertThrows(IllegalArgumentException.class, () -> {new RSVPCountryInfo("us", 0);});

		final RSVPCountryInfo rsvpCountryInfo = new RSVPCountryInfo("us", 1);
		Assertions.assertNotNull(rsvpCountryInfo, "RSVPCountryInfo must not be null.");
		Assertions.assertEquals(rsvpCountryInfo.getCountry(), "us");
		Assertions.assertEquals(rsvpCountryInfo.getCount(), 1);
    }
}
