/**
 * Copyright 2020 Jake Waffle
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.waffle.jake.please.respond.rsvp;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.waffle.jake.please.respond.rsvp.dto.Event;
import com.waffle.jake.please.respond.rsvp.dto.Group;
import com.waffle.jake.please.respond.rsvp.dto.RSVP;
import com.waffle.jake.please.respond.rsvp.dto.adapters.Modules;
import com.waffle.jake.please.respond.rsvp.mock.MeetupClientMock;

public class MeetupAggregatorTest {
	private static final int TIME_INTERVAL_MILLIS = 5_000;
	private static final String TEST_EVENT_URL = "http://www.some-event.com/blah/blah/blah";

	/**
	 *  Immutable Mapper (thread-safe)
	 */
	private static final ObjectMapper MAPPER = new ObjectMapper()
			.registerModule(Modules.rsvp());
	
	private List<RSVP> getTestRSVPs(final int count, final Instant startTime, final List<String> rsvpCountries) {
		Assertions.assertEquals(count, rsvpCountries.size());
		
		final List<String> randomRSVPCountries = new ArrayList<>(rsvpCountries);
		// We shuffle the rsvp countries to make the tests a bit more random.
		Collections.shuffle(randomRSVPCountries);

		Instant currentTime = startTime;
		final List<RSVP> rsvps = new ArrayList<>();
		for (int i = 0; i < count; i++) {
			rsvps.add(RSVP.create(
					Event.create(currentTime, TEST_EVENT_URL),
					Group.create(randomRSVPCountries.get(i))));
			
			// Moves the event time using the interval millis.
			currentTime = currentTime.plusMillis(TIME_INTERVAL_MILLIS);
		}
		return rsvps;
	}
	
	private InputStream createFakeStream(final List<RSVP> rsvps) {
		final List<String> rsvpJsons = rsvps.stream()
                .map(rsvp -> {
					try {
						return MAPPER.writeValueAsString(rsvp);
					} catch (JsonProcessingException e) {
						throw new IllegalStateException(e);
					}
				})
                .collect(Collectors.toList());

        final String fakeRSVPsResponse = String.join("", rsvpJsons);
        return new ByteArrayInputStream(fakeRSVPsResponse.getBytes(StandardCharsets.UTF_8));
	}
	
	@Test
	public void testEmptyStream() {
		final InputStream fakeRSVPStream = this.createFakeStream(Collections.emptyList());
		final MeetupAggregator aggregator = new MeetupAggregator(new MeetupClientMock(fakeRSVPStream));
		
		final String[] aggregationInfo = aggregator.getRSVPAggegationInfo(3).split(",");
		Assertions.assertEquals(aggregationInfo.length, 9, "There must be 9 fields in the aggregated info.");
		Assertions.assertEquals(aggregationInfo[0], "0", "The total RSVPS must be 0.");
		
		// All other fields will be 'n/a' because the rsvp stream was empty.
		for (int i = 1; i < 9; i++) {
			Assertions.assertEquals(aggregationInfo[i], "n/a", "This field must be 'n/a'.");
		}
	}

	@Test
	public void testOneCountryStream() {
		final int rsvpCount = 5;
		final Instant startTime = Instant.ofEpochMilli(System.currentTimeMillis());
        final List<RSVP> rsvps = this.getTestRSVPs(rsvpCount, startTime, List.of("us", "us", "us", "us", "us"));
		
		final InputStream fakeRSVPStream = this.createFakeStream(rsvps);
		final MeetupAggregator aggregator = new MeetupAggregator(new MeetupClientMock(fakeRSVPStream));
		
		final String[] aggregationInfo = aggregator.getRSVPAggegationInfo(3).split(",");
		Assertions.assertEquals(aggregationInfo.length, 9, "There must be 9 fields in the aggregated info.");
		Assertions.assertEquals(aggregationInfo[0], Integer.toString(rsvpCount), "The total RSVPS must be correct.");
		
		final RSVP lastRSVP = rsvps.get(rsvps.size() - 1);
		Assertions.assertEquals(aggregationInfo[1], DateTimeFormatter.ISO_INSTANT.format(lastRSVP.getEvent().getTime()), "Latest RSVP date must be correct.");
		Assertions.assertEquals(aggregationInfo[2], TEST_EVENT_URL, "Latest RSVP URL must be correct.");
		Assertions.assertEquals(aggregationInfo[3], "us", "First country should be 'us'.");
		Assertions.assertEquals(aggregationInfo[4], "5", "First country count should be 5.");

		// All other fields should be 'n/a'.
		for (int i = 5; i < 9; i++) {
			Assertions.assertEquals(aggregationInfo[i], "n/a", "This field must be 'n/a'.");
		}
	}

	@Test
	public void testTwoCountryStream() {
		final int rsvpCount = 5;
		final Instant startTime = Instant.ofEpochMilli(System.currentTimeMillis());
        final List<RSVP> rsvps = this.getTestRSVPs(rsvpCount, startTime, List.of("us", "us", "us", "de", "de"));
		
		final InputStream fakeRSVPStream = this.createFakeStream(rsvps);
		final MeetupAggregator aggregator = new MeetupAggregator(new MeetupClientMock(fakeRSVPStream));
		
		final String[] aggregationInfo = aggregator.getRSVPAggegationInfo(3).split(",");
		Assertions.assertEquals(aggregationInfo.length, 9, "There must be 9 fields in the aggregated info.");
		Assertions.assertEquals(aggregationInfo[0], Integer.toString(rsvpCount), "The total RSVPS must be correct.");
		
		final RSVP lastRSVP = rsvps.get(rsvps.size() - 1);
		Assertions.assertEquals(aggregationInfo[1], DateTimeFormatter.ISO_INSTANT.format(lastRSVP.getEvent().getTime()), "Latest RSVP date must be correct.");
		Assertions.assertEquals(aggregationInfo[2], TEST_EVENT_URL, "Latest RSVP URL must be correct.");
		Assertions.assertEquals(aggregationInfo[3], "us", "First country should be 'us'.");
		Assertions.assertEquals(aggregationInfo[4], "3", "First country count should be 3.");;
		Assertions.assertEquals(aggregationInfo[5], "de", "Second country should be 'de'.");
		Assertions.assertEquals(aggregationInfo[6], "2", "Second country count should be 2.");
		
		// All other fields should be 'n/a'.
		for (int i = 7; i < 9; i++) {
			Assertions.assertEquals(aggregationInfo[i], "n/a", "This field must be 'n/a'.");
		}
	}

	@Test
	public void testThreeCountryStream() {
		final int rsvpCount = 6;
		final Instant startTime = Instant.ofEpochMilli(System.currentTimeMillis());
        final List<RSVP> rsvps = this.getTestRSVPs(rsvpCount, startTime, List.of("us", "us", "us", "de", "de", "ru"));
		
		final InputStream fakeRSVPStream = this.createFakeStream(rsvps);
		final MeetupAggregator aggregator = new MeetupAggregator(new MeetupClientMock(fakeRSVPStream));
		
		final String[] aggregationInfo = aggregator.getRSVPAggegationInfo(3).split(",");
		Assertions.assertEquals(aggregationInfo.length, 9, "There must be 9 fields in the aggregated info.");
		Assertions.assertEquals(aggregationInfo[0], Integer.toString(rsvpCount), "The total RSVPS must be correct.");
		
		final RSVP lastRSVP = rsvps.get(rsvps.size() - 1);
		Assertions.assertEquals(aggregationInfo[1], DateTimeFormatter.ISO_INSTANT.format(lastRSVP.getEvent().getTime()), "Latest RSVP date must be correct.");
		Assertions.assertEquals(aggregationInfo[2], TEST_EVENT_URL, "Latest RSVP URL must be correct.");
		Assertions.assertEquals(aggregationInfo[3], "us", "First country should be 'us'.");
		Assertions.assertEquals(aggregationInfo[4], "3", "First country count should be 3.");
		Assertions.assertEquals(aggregationInfo[5], "de", "Second country should be 'de'.");
		Assertions.assertEquals(aggregationInfo[6], "2", "Second country count should be 2.");
		Assertions.assertEquals(aggregationInfo[7], "ru", "Third country should be 'ru'.");
		Assertions.assertEquals(aggregationInfo[8], "1", "Third country count should be 1.");
	}

	@Test
	public void testFourCountryStream() {
		final int rsvpCount = 10;
		final Instant startTime = Instant.ofEpochMilli(System.currentTimeMillis());
        final List<RSVP> rsvps = this.getTestRSVPs(rsvpCount, startTime, List.of("us", "us", "us", "us", "de", "de", "de", "ru", "ru", "ze"));
		
		final InputStream fakeRSVPStream = this.createFakeStream(rsvps);
		final MeetupAggregator aggregator = new MeetupAggregator(new MeetupClientMock(fakeRSVPStream));
		
		final String[] aggregationInfo = aggregator.getRSVPAggegationInfo(3).split(",");
		Assertions.assertEquals(aggregationInfo.length, 9, "There must be 9 fields in the aggregated info.");
		Assertions.assertEquals(aggregationInfo[0], Integer.toString(rsvpCount), "The total RSVPS must be correct.");
		
		final RSVP lastRSVP = rsvps.get(rsvps.size() - 1);
		Assertions.assertEquals(aggregationInfo[1], DateTimeFormatter.ISO_INSTANT.format(lastRSVP.getEvent().getTime()), "Latest RSVP date must be correct.");
		Assertions.assertEquals(aggregationInfo[2], TEST_EVENT_URL, "Latest RSVP URL must be correct.");
		Assertions.assertEquals(aggregationInfo[3], "us", "First country should be 'us'.");
		Assertions.assertEquals(aggregationInfo[4], "4", "First country count should be 4.");
		Assertions.assertEquals(aggregationInfo[5], "de", "Second country should be 'de'.");
		Assertions.assertEquals(aggregationInfo[6], "3", "Second country count should be 3.");
		Assertions.assertEquals(aggregationInfo[7], "ru", "Third country should be 'ru'.");
		Assertions.assertEquals(aggregationInfo[8], "2", "Third country count should be 2.");
		
		// The 'ze' country is omitted because only up to 3 countries are presented in the aggregation info.
	}
}
