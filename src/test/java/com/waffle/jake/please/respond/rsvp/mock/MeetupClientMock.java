/**
 * Copyright 2020 Jake Waffle
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.waffle.jake.please.respond.rsvp.mock;

import java.io.InputStream;

import com.waffle.jake.please.respond.rsvp.IMeetupClient;

public final class MeetupClientMock implements IMeetupClient {
    private final InputStream rsvpStream;
    
	public MeetupClientMock(final InputStream rsvpStream) {
		this.rsvpStream = rsvpStream;
	}
	
	/**
	 * Returns a fake rsvp stream.
	 * 
	 * {@inheritDoc}}
	 */
	@Override
	public InputStream getRSVPStream() {
		return this.rsvpStream;
	}

}
