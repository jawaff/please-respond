/**
 * Copyright 2020 Jake Waffle
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.waffle.jake.please.respond;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.opentest4j.AssertionFailedError;

// Note: Remove this to enable the integration tests.
@Disabled
public class MainTest {
	/**
	 * This regex pattern should match the expected output of the program.
	 */
	private static final Predicate<String> OUTPUT_TESTER = Pattern.compile("^([0-9]+),([^,]+),([^,]+)(,(n\\/a|[a-zA-Z]{2}),(n\\/a|[0-9]+)){3}$")
			.asMatchPredicate();
	
	private final PrintStream standardOut = System.out;
	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

	@BeforeEach
	public void setUp() {
		// Redirects standard out to our output stream.
	    System.setOut(new PrintStream(outputStreamCaptor));
	}
	
	@AfterEach
	public void tearDown() {
		// Resets the standard out stream.
	    System.setOut(standardOut);
	}
	
	/**
	 * This is our unreliable, black box test.
	 * @param collectionSeconds The amount of seconds to wait for RSVPs.
	 */
	@ParameterizedTest
	@ValueSource(ints = {10, 60})
    public void testMain(final int collectionSeconds) {
		int retryCount = 0;
		boolean shouldRetry = true;
		
		// We need to retry the RSVP collection due to the fact that we're relying on the response speed of the Meetup.com server, our internet's latency and the processing speed of RSVPs.
		while (shouldRetry && retryCount < 3) {
		    try {
    	        final long startMillis = System.currentTimeMillis();
    	        
    	        // 60 is the default, so we'll omit the argument to make sure the default works correctly.
    	        if (collectionSeconds == 60) {
        	        Main.main();
    	        } else {
        	        Main.main(Integer.toString(collectionSeconds));	
    	        }
    	        
    	        final long elapsedMillis = System.currentTimeMillis() - startMillis;
    	        final long collectionMillis = collectionSeconds * 1_000;
    	        
    	        // We test that elapsedMillis and collectionMillis are "close".
    	        // Due to how unreliable this sort of test is, we use a large time window.
    	        // Note that the time window is smaller than the gap between the test parameters.
    	        Assertions.assertTrue(elapsedMillis - 9_000 <= collectionMillis, String.format("Elapsed time fails lower bound test: %d <= %d", elapsedMillis - 9_000, collectionMillis));
    	        Assertions.assertTrue(collectionMillis <= elapsedMillis + 500, String.format("Elapsed time fails upper bound test: %d <= %d", collectionMillis, elapsedMillis + 500));
    	        
    	        // If we pass the time dependent tests, then we move onto the next part of the test.
    	        shouldRetry = false;
		    } catch (AssertionFailedError e) {
                // If we must retry, then we need to wipe the captured stdout stream.
		    	outputStreamCaptor.reset();
		    	retryCount += 1;
		    	
		    	// We sleep a bit so that Meetup.com's server doesn't think we're DoSing it.
		    	try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
                    Thread.currentThread().interrupt();
                    throw new IllegalStateException();
				}
		    }
		}
		
		// shouldRetry should be false if the time assertions have passed.
		if (shouldRetry) {
			throw new AssertionError("Failed too many times to collect RSVPs within the expected time window (this may still be a false positive.)");
		}
    	
    	final String capturedStdout = outputStreamCaptor.toString().trim();
    	Assertions.assertFalse(capturedStdout.isBlank(), "Expected stdout to be nonempty.");
    	Assertions.assertTrue(OUTPUT_TESTER.test(capturedStdout), String.format("Stdout doesn't match our test regular expression: %s", capturedStdout));
	}
}
