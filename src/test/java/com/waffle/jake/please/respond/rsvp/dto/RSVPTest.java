/**
 * Copyright 2020 Jake Waffle
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.waffle.jake.please.respond.rsvp.dto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.ValueInstantiationException;
import com.waffle.jake.please.respond.rsvp.dto.adapters.Modules;

public class RSVPTest {
	/**
	 *  Immutable Mapper (thread-safe)
	 */
	private static final ObjectMapper MAPPER = new ObjectMapper()
			.registerModule(Modules.rsvp());
	
    @Test
    public void deserializeTest() {
    	Assertions.assertThrows(ValueInstantiationException.class, () -> {MAPPER.readValue("{}", RSVP.class);});
    	Assertions.assertThrows(ValueInstantiationException.class, () -> {MAPPER.readValue("{\"event\":{\"time\": 123, \"event_url\":\"asdf\"}}", RSVP.class);});
    	Assertions.assertThrows(ValueInstantiationException.class, () -> {MAPPER.readValue("{\"group\":{\"group_country\": \"us\"}}", RSVP.class);});

    	try {
			final RSVP rsvp = MAPPER.readValue("{\"event\":{\"time\": 123, \"event_url\":\"asdf\"}, \"group\":{\"group_country\": \"us\"}}", RSVP.class);
			Assertions.assertNotNull(rsvp, "Deserialized rsvp must not be null.");
			Assertions.assertNotNull(rsvp.getEvent(), "RSVP Event must not be null.");
			Assertions.assertNotNull(rsvp.getGroup(), "RSVP Group must not be null.");
		} catch (JsonProcessingException e) {
			throw new AssertionError("Failed to deserialize valid Group json.", e);
		}
    }
}
